# mirror_agent.py - Tianwei Ni
# based on hatTSF.py

from .hatTSF import HatTSF
import numpy as np
import time
np.set_printoptions(precision=2, suppress=True)

class Mirror_Shooter(HatTSF):
	def __init__(self, player, id=1, attack_dist=20):
		super().__init__(player, id)
		self.attack_dist = attack_dist

	def update(self):
		if not self.hasStateChanged:
			return
		
		state = self.state_queue[-1].copy() # rightmost/current state
		agent_state = state[self.id*6 + 4: self.id*6 + 10].copy()
		human_state = state[(1-self.id)*6 + 4: (1-self.id)*6 + 10]

		if state[1] and human_state[-1] and state[2] != self.id: # both alive and not be target
			if state[2] == 1 - self.id or np.linalg.norm(agent_state[:2]) > 220: # radius
				turn, thrust = self.mirror_pos(human_state, agent_state)
				fire = self.rule_based_missile(state, agent_state)
			else: # no target and very close to region: wait
				turn, thrust, fire = 0, 0, 0
		elif self.inRegion(agent_state[:2], r=220):
			fire = 0
			turn, thrust = self.leave_out(agent_state)
		else:
			turn, thrust, fire = 0, 0, 0
		
		action_array = np.array([turn, thrust, fire])

		self.emit_command(action_array)

	def mirror_pos(self, human_state, agent_state):
		human_pos = human_state[:2]
		human_angle = human_state[3]
		agent_pos = agent_state[:2]
		agent_velo = agent_state[3:5]
		agent_angle = agent_state[2]
		target_pos = -human_pos
		target_angle = self.position_angle(target_pos[0] - agent_pos[0], target_pos[1] - agent_pos[1]) / np.pi * 180 # towards target

		min_dist = np.inf
		best_turn = 0
		for turn in [0, 1, -1]:
			dist = self.pointing_angle_diff(target_angle, agent_angle + turn * 8)
			if dist < min_dist:
				min_dist = dist
				best_turn = turn

		min_dist = np.inf
		best_thrust = 0
		for thrust in [0, 1]:
			next_pos = self.simulate_move(agent_pos, agent_velo, agent_angle, best_turn, thrust)
			dist = self._dist_from_player(target_pos, next_pos)
			if dist < min_dist:
				min_dist = dist
				best_thrust = thrust
		best_turn = 2 if best_turn == -1 else best_turn

		if min_dist > self.attack_dist:
			return best_turn, best_thrust
		else: # if close to target, then aim towards fortress for shooting
			target_angle = self.position_angle(-agent_pos[0], -agent_pos[1]) / np.pi * 180 # towards fortress
			min_dist = np.inf
			best_turn = 0
			for turn in [0, 1, -1]:
				dist = self.pointing_angle_diff(target_angle, agent_angle + turn * 8)
				if dist < min_dist:
					min_dist = dist
					best_turn = turn
# 			best_turn = 2 if best_turn == -1 else best_turn
			return best_turn, 0 # no thrust

	def get_log_prob(self, state, action, turn_only=False):
		agent_state = state[self.id*6 + 4: self.id*6 + 10].copy()
		human_state = state[(1-self.id)*6 + 4: (1-self.id)*6 + 10]

		if state[1] and human_state[-1] and state[2] != self.id: # both alive and not be target
			if state[2] == 1 - self.id or np.linalg.norm(agent_state[:2]) > 220: # radius
				turn, thrust = self.mirror_pos(human_state, agent_state)
				fire = self.rule_based_missile(state, agent_state)
			else: # no target and very close to region: wait
				turn, thrust, fire = 0, 0, 0
		elif self.inRegion(agent_state[:2], r=220):
			fire = 0
			turn, thrust = self.leave_out(agent_state)
		else:
			turn, thrust, fire = 0, 0, 0
	
		return (1.0 - (turn == action[0])) * np.log(self.eps) + (1.0 - (thrust == action[1])) * np.log(self.eps)

	def get_action_dist(self, state):
		# Shooter
		# action space: turn {3} x thrust {2} x fire {2}
		action_dist = np.zeros((3, 2, 2))

		for turn in range(3):
			for thrust in range(2):
				for fire in range(2):
					action_dist[turn, thrust, fire] = self.get_log_prob(state, np.array([turn, thrust, fire]))

		return action_dist

