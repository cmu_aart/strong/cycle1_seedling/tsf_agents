from .hatTSF import HatTSF
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import time
from collections import OrderedDict
np.set_printoptions(precision=2, suppress=True)

base = np.array([
	[0, 0, 0],
	[0, 0, 1],
	[0, 1, 0],
	[0, 1, 1],
	[1, 0, 0],
	[1, 0, 1],
	[1, 1, 0],
	[1, 1, 1],
	[2, 0, 0],
	[2, 0, 1],
	[2, 1, 0],
	[2, 1, 1],
]) # (12, 3)


def multidiscrete2scalar(a):
	# a: np.array (*, 3)
	# out: np.array (*)
	out = 4 * a[..., 0] + 2 * a[..., 1] + a[..., 2]
	# 4 * turn + 2 * thrust + 1 * fire ~ [0, 11]
	return out

def scalar2multidiscrete(a):
	# a: np.array (*) or scalar
	# out: np.array (*, 3)
	out = base[a] 
	return out

def mlp(sizes, activation, output_activation=nn.Identity, layernorm=False):
	layers = []
	for j in range(len(sizes)-1):
		act = activation if j < len(sizes)-2 else output_activation
		layers.append(nn.Linear(sizes[j], sizes[j+1]))
		if layernorm:
			layers.append(nn.LayerNorm(sizes[j+1]))
		layers.append(act())
	return nn.Sequential(*layers)

class Shooter_SQIL(HatTSF):
	'''
	Q function: (*, S) -> (*, A=12) -> (*)
	'''
	def __init__(self, player, shooter_type, id=1, drag=0.05, 
					hidden_sizes=(256,256), 
					activation=nn.ReLU, layernorm=False, **kwargs):
		super().__init__(player, id, drag)

		self.obs_dim = 28
		self.act_dim = 12

		self.q = mlp([self.obs_dim] + list(hidden_sizes) + [self.act_dim], 
					activation, layernorm=layernorm)
		
		if shooter_type == 1:
			# mix
			state_dict = torch.load(f'{self.model_path}/shooter_sqil/ret0.9_r10_time14s_step481950.pt')
		elif shooter_type == 2:
			# pure RL
			state_dict = torch.load(f'{self.model_path}/shooter_sqil/ret0.90_r1_time13s_step452250.pt')
		elif shooter_type == 3: 
			# pure IL
			state_dict = torch.load(f'{self.model_path}/shooter_sqil/ret1.00_r0_time18s_step471150.pt')
		elif shooter_type == 4:
			# mix
			state_dict = torch.load(f'{self.model_path}/shooter_sqil/ret1.00_r10_time9s_step232200.pt')
	
		new_state_dict = OrderedDict()
		for k, v in state_dict.items():
			name = k[2:] # remove `q.`
			new_state_dict[name] = v
		self.q.load_state_dict(new_state_dict)

	def forward(self, obs, act):
		'''
		input: obs: (*, X) tensor
		act: (*,) tensor
		return: (*,) tensor
		'''
		q = self.q(obs) # (*, A)
		batch_size = obs.shape[0]
		q = q[np.arange(batch_size), act.long()] # (*)
		return q # Critical to ensure q has right shape.

	@torch.no_grad()
	def softmax(self, obs):
		'''
		softmax policy for rollout
		input: obs: (*, X) tensor
		return: act: (*, A) tensor
		'''
		q = self.q(obs) # (*, A)
		return F.softmax(q, dim=-1) # (*, A)

	@torch.no_grad()
	def log_sum_exp(self, obs):
		'''
		for training Q as target
		input: obs: (*, X) tensor
		return: logit: (*) tensor
		'''
		q = self.q(obs) # (*, A)
		return torch.logsumexp(q, dim=-1) # (*)

	def update(self):
		if not self.hasStateChanged:
			return

		state = self.state_queue[-1].copy() # rightmost/current state
		agent_state = state[self.id*6 + 4: self.id*6 + 10].copy()
		human_state = state[(1-self.id)*6 + 4: (1-self.id)*6 + 10].copy()

		if state[1] and human_state[-1] and state[2] != self.id: # both alive and not be target
			if state[2] == 1 - self.id or np.linalg.norm(agent_state[:2]) > 220: # radius
				turn, thrust, fire = self.real_act(state)
			else: # no target and very close to region: wait
				turn, thrust, fire = 0, 0, 0
		elif self.inRegion(agent_state[:2], r=220):
			fire = 0
			turn, thrust = self.leave_out(agent_state)
		else:
			turn, thrust, fire = 0, 0, 0

		action_array = np.array([turn, thrust, fire])
		self.emit_command(action_array)

	def preprocess(self, state):
		obs = state.copy()

		if self.id == 1:
			agent_state = obs[self.id*6 + 4: self.id*6 + 10].copy()
			human_state = obs[(1-self.id)*6 + 4: (1-self.id)*6 + 10].copy()

			# transform the state as input
			obs[self.id*6 + 4: self.id*6 + 10] = human_state # bait state
			obs[(1-self.id)*6 + 4: (1-self.id)*6 + 10] = agent_state # shooter state
			if obs[2] == self.id:
				obs[2] = 1- self.id
			elif obs[2] == 1- self.id:
				obs[2] = self.id

		if not torch.is_tensor(obs):
			obs = torch.as_tensor(obs, dtype=torch.float32)
		if obs.ndim == 1:
			obs = obs.unsqueeze(0) # (1, S)
		
		return obs
		
	def real_act(self, state, deterministic=True):
		obs = self.preprocess(state)
		
		logits = self.softmax(obs) # (*, A)
		if deterministic:
			a = torch.argmax(logits, dim=-1) # (*,)
		else:
			a = torch.multinomial(logits, 1) # (*, 1)
		
		if obs.shape[0] == 1:
			a = a.item()
		else:
			a = a.data.numpy().flatten() # (*, )
		
		print(logits, a)
		return scalar2multidiscrete(a)

	@torch.no_grad()
	def get_log_prob(self, state, act):
		'''
		bc interface
		input: obs: (X) array
		act: multidiscrete array
		'''
		obs = self.preprocess(state)
		logits = self.softmax(obs).squeeze(0) # (A)

		act = multidiscrete2scalar(act)
		return np.log(logits[act].item()+1e-8)