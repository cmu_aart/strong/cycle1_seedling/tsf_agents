from .hatTSF import HatTSF
import numpy as np

class Zero_Agent(HatTSF):
	def __init__(self, player, id=1, drag=0.05):
		super().__init__(player, id, drag)
		
	def update(self):
		if not self.hasStateChanged:
			return

		action_array = np.array([0, 0, 0])
		self.emit_command(action_array)

