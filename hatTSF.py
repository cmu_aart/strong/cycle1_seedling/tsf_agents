# hatTSF.py - Tianwei Ni
# translated from spacefortress_agent.cpp by Dana Hughes
# call it in server.py

import pyTSF as tsf
import numpy as np
from gym import spaces
import json
import time
from threading import Lock
from collections import deque
from .utils import getStateDict, getStateArray

class HatTSF(tsf.CallbackAgent):
	''' inherit from C++ CallbackAgent class by Dana Hughes.
		required to implement two methods:
		- onReceiveGameState: called by notify() implicitly, stored in self.gameState
		- update: called by run() implicitly, pass command to self.player
	'''
	def __init__(self, player, id=1, drag=0.05):
		super().__init__()
		self.player = player
		assert id in [0, 1]
		self.id = id
		self.hasStateChanged = False
		self.turn_list = [tsf.NO_TURN, tsf.TURN_LEFT, tsf.TURN_RIGHT]
		self.drag = drag
		self.eps = 1e-8

		self.model_path = 'agents/models'

		self.observation_space = spaces.Box(low=-np.inf, high=np.inf, shape=(28, ))  # fully-observable
		self.action_space = spaces.MultiDiscrete([3, 2, 2])  # single-agent

		self.state_lock = Lock()
		self.state_queue = deque(maxlen=2) # last and current state
		self.action_queue = deque(maxlen=1) # last action

	def onReceiveGameState(self):
		with self.state_lock:
			state_array, action_array = self._getStateArray()
			self.state_queue.append(state_array)
			self.action_queue.append(action_array)
			self.hasStateChanged = True
				
	def update(self):
		'''
		if not self.hasStateChanged:
			return
		action_array = ...
		self.emit_command(action_array)
		'''
		return NotImplementedError

	def get_log_prob(self, state, action, turn_only=False):
		return NotImplementedError

	def emit_command(self, action_array):
		"""
		action_array: np.array(3)
		actions - a pair of triples representing each player's action.  Individual action triples follow the schema
			action - (turn, thrust, fire)
				turn:   0 = NO_TURN, 1 = TURN_LEFT, 2 = TURN_RIGHT
				thrust: boolean
				fire:   boolean
		"""
		command = tsf.PlayerCommand()
		command.turn = self.turn_list[action_array[0]]
		command.thrust = True if action_array[1] else False
		command.fire = True if action_array[2] else False
		self.player.command(command)
		self.hasStateChanged = False

	def command2array(self, command):
		action_array = np.zeros((3,))
		if command.turn == tsf.NO_TURN:
			action_array[0] = 0
		elif command.turn == tsf.TURN_LEFT:
			action_array[0] = 1
		else:
			action_array[0] = 2
		action_array[1] = 1 if command.thrust else 0
		action_array[2] = 1 if command.fire else 0
		return action_array

	def __getStateDict(self):
		self.state_dict = getStateDict(self.gameState)

	def _getStateArray(self, shells=True):
		self.__getStateDict()
		state_array, action_array = getStateArray(self.state_dict, self.id, shells) 
		# bait_id = agent_id, cuz shooter agent does not care shells
		return state_array, action_array

	def _dist_from_player(self, player_pos, projectile_pos):
		return np.linalg.norm(player_pos - projectile_pos)


	### Some rule-based methods for common use. 
	### Tianwei, Yikang

	# 0. whether inside the region
	def inRegion(self, pos, r=220): 
		x, y = np.abs(pos)
		if x > np.sqrt(3) / 2 * r:
			return False
		return y < r - x / np.sqrt(3)

	# 1. leave out the region when object is killed. For both roles
	def leave_out(self, agent_state):
		agent_pos = agent_state[:2]
		agent_velo = agent_state[3:5]
		agent_angle = agent_state[2]
		pos_angle = self.position_angle(agent_pos[0], agent_pos[1]) / np.pi * 180
		min_dist = np.inf
		for turn in [0, 1, -1]:
			dist = self.pointing_angle_diff(pos_angle, agent_angle + turn * 8)
			if dist < min_dist:
				min_dist = dist
				best_turn = turn

		max_dist = -np.inf
		best_thrust = 0
		for thrust in [0, 1]:
			next_pos = self.simulate_move(agent_pos, agent_velo, agent_angle, best_turn, thrust)
			dist = np.linalg.norm(next_pos)
			if dist > max_dist:
				max_dist = dist
				best_thrust = thrust
		best_turn = 2 if best_turn == -1 else best_turn
		return best_turn, best_thrust
	
	# 2. enter the region when object respawn. For bait
	def enter_in(self, agent_state):
		agent_pos = agent_state[:2]
		agent_velo = agent_state[3:5]
		agent_angle = agent_state[2]
		pos_angle = self.position_angle(agent_pos[0], agent_pos[1]) / np.pi * 180
		target_angle = pos_angle + 180
		if target_angle > 360:
			target_angle -= 360
		
		min_dist = np.inf
		for turn in [0, 1, -1]:
			dist = self.pointing_angle_diff(target_angle, agent_angle + turn * 8)
			if dist < min_dist:
				min_dist = dist
				best_turn = turn

		min_dist = np.inf
		best_thrust = 0
		for thrust in [0, 1]:
			next_pos = self.simulate_move(agent_pos, agent_velo, agent_angle, best_turn, thrust)
			dist = np.linalg.norm(next_pos)
			if dist < min_dist:
				min_dist = dist
				best_thrust = thrust
		best_turn = 2 if best_turn == -1 else best_turn
		return best_turn, best_thrust

	# 3. shoot missile. For shooter
	def rule_based_missile(self, state, agent_state, valid_pos_angle=np.pi/3, valid_point_angle=60, dist_thres=10):
		''' follow Yikang's method in agent-agent teaming
		'''
		x, y = agent_state[:2]
		dist = np.sqrt(x**2 + y**2) # shooter's distance to origin
		if state[3] == 0 or state[1] == 0:	# fortress is NOT vulnerable or dead
			return 0
		if dist >= 145:		# too far away: missile_speed * missile_duration = 300 * 0.5 = 150
			return 0
		
		k = np.tan(agent_state[2] / 180 * np.pi)
		b = y - k * x
		zero_point_dist = abs(b) / np.sqrt(k ** 2 + 1)	
		theta = self.position_angle(x, y)
		
		if zero_point_dist < dist_thres and self.pointing_angle_diff(state[0], agent_state[2]) < valid_point_angle:
			if np.pi - valid_pos_angle < abs(state[0] / 180 * np.pi - theta) < np.pi + valid_pos_angle:
				return 1
		return 0
	
	# Other utility methods
	def pointing_angle_diff(self, x, y):
		diff = abs(x - y)
		return min(diff, 360 - diff)

	def position_angle(self, x, y):
		# compute the absolute angle from the east. [0, 2pi]
		if x > 0:
			theta = np.arctan(y / x)
			if y < 0:
				theta += np.pi * 2
		elif x < 0:
			theta = np.arctan(y / x) + np.pi
		else:
			if y >= 0:
				theta = np.pi / 2
			else:
				theta = np.pi * 3 / 2
		return theta

	def simulate_move(self, agent_pos, agent_velo, agent_angle, turn, thrust):
		turn = -1 if turn == 2 else turn
		next_angle = (agent_angle + turn * 8) / 180 * np.pi
		next_velo = agent_velo * (1 - self.drag)
		if thrust:
			next_velo += np.array([np.cos(next_angle), np.sin(next_angle)]) * 3
			if np.linalg.norm(next_velo) > 180:
				next_velo = next_velo / np.linalg.norm(next_velo) * 180
		return agent_pos + next_velo / 30	


